//: Playground - noun: a place where people can play

import UIKit

import Foundation

// Initial variables (just to get you started)
var numberOfPlayers = 3
var playersPositions: [Int] = [Int](count: numberOfPlayers, repeatedValue: 0) //[0,0,0]
var snakeBoard: [Int] = []

// Functions
func boardSetup() {
    var place: Int = 0
    while place < 100 {
        snakeBoard.append (0)
        place = place + 1
    }
    // Boring board
    snakeBoard
    
    // Fun board
    snakeBoard[6] = 15
    snakeBoard[9] = -3
    snakeBoard[2] = -3
    snakeBoard[16] = 3
    snakeBoard[9] = 20
    snakeBoard[20] = 10
    snakeBoard[25] = -10
    snakeBoard[95] = -23
    snakeBoard[98] = -40
    snakeBoard[97] = -80
    snakeBoard[96] = 20
    snakeBoard[99] = 32
    snakeBoard[88] = -50
    
}



func rollDice() ->Int{
    // Generate a random number between 1 to 6
    // Hint: use the rand() function to get a random number, then limit it to between 1 to 6
    let randomDice: Int = Int((rand() % 6) + 1)
    print("Dice random rolled: \(randomDice)")
    
    return randomDice
}

// Pass it a player number, rolled dice, etc
// and print out a description of the move
func printStep() {
    print("Player number \(numberOfPlayers) rolled a dice at number \(snakeBoard)")
    
    
}

boardSetup()
var hasWinner: Bool = false

print("Start running")
var round:  Int = 0


while (hasWinner == false) {
    round =  round + 1
    print("Round \(round)")
    playerTurns: for index in 1...numberOfPlayers {
        let initialPlayerPosition: Int = playersPositions[index - 1]
        
        print("Player \(index) is at \(initialPlayerPosition)" )
        if playersPositions[index - 1] < 100 {
            playersPositions[index - 1] = initialPlayerPosition + rollDice()
            
            if playersPositions[index - 1] < 100 {
                var bonusMove:Int = snakeBoard[playersPositions[index - 1]]
                
                print("Checking bonus for \(playersPositions[index - 1]): Should add \(bonusMove)")
                playersPositions[index - 1] += bonusMove
            }
            if playersPositions[index - 1] > 100 {
                //playersPositions[index - 1] = 100
                var extraSteps = playersPositions[index - 1] - 100
                playersPositions[index - 1 ] -= (extraSteps*2)
            }
            print("Player \(index) is now at \(playersPositions[index - 1])")
            if playersPositions[index - 1] >= 100 {
                
                print("Player \(index) is the winner")
                hasWinner = true
                break
            }
            
        }
    }
    // hello adam
    // Check for winners
    //Repeat until winner is found
    
    
}
//: [Next](@next)

